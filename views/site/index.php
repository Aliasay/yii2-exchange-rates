<?php

/* @var $this yii\web\View */

$this->title = 'Exchange Rates';
?>
<div class="site-index">
    <h1><?= $this->title ?></h1>
    <?php

    echo \galiasay\exchange\widgets\ExchangeRates::widget([
        'type' => 'line',
        'currency' => $currency,
        'options' => [
        ],
        'clientOptions' => [
            'scales' => [
                'xAxes' => [
                    [
                        'type' => 'time',
                        'time' => [
                            'unit' => 'day',
                        ]
                    ]
                ],
                'yAxes' => [
                    [
                        'ticks' => [
                            'beginAtZero' => true
                        ]
                    ]
                ]
            ]
        ],
    ]);

    ?>
</div>
