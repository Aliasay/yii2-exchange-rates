<?php

namespace galiasay\exchange\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `exchange_rates`.
 */
class m170612_210834_create_exchange_rates_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('exchange_rates', [
            'id' => $this->primaryKey(),
            'currency' => $this->string(3)->notNull(),
            'rate' => $this->decimal(10, 2)->notNull(),
            'date' => $this->date()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('exchange_rates');
    }
}
