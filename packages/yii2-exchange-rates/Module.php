<?php

namespace galiasay\exchange;

use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @var string
     */
    public $currency = 'USD';

    /**
     * @var string
     */
    public $url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'galiasay\exchange\commands';
        }
    }
}