<?php

namespace galiasay\exchange\widgets;

use app\assets\ChartAsset;
use dosamigos\chartjs\ChartJs;
use galiasay\exchange\assets\MomentAsset;
use galiasay\exchange\models\ExchangeRate;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

class ExchangeRates extends Widget
{
    /**
     * @var array
     */
    public $clientOptions = [];

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var string
     */
    public $type;

    /**
     * @var
     */
    public $currency = 'USD';


    /**
     * Renders the widget.
     */
    public function run()
    {
        $view = $this->getView();
        MomentAsset::register($view);

        $rates = $this->getRates();

        return ChartJs::widget([
            'type' => $this->type,
            'clientOptions' => $this->clientOptions,
            'options' => $this->options,
            'data' => [
                'labels' => array_column($rates, 'date'),
                'datasets' => [
                    [
                        'label' => $this->currency,
                        'backgroundColor' => 'rgba(33, 150, 243, .1)',
                        'borderColor' => 'rgba(33,150,243, 1)',
                        'data' => array_column($rates, 'rate')
                    ],
                ]
            ],
        ]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getRates()
    {
        return ExchangeRate::find()
            ->where(['currency' => $this->currency])
            ->orderBy('date')
            ->asArray()
            ->all();
    }
}