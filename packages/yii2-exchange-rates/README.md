Exchange Rate
=============
Exchange Rate

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist galiasay/yii2-exchange-rates "*"
```

or add

```
"galiasay/yii2-exchange-rates": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```
<?= \galiasay\exchange\widgets\ExchangeRates::widget(); ?>
```
