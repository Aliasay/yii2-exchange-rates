<?php

namespace galiasay\exchange\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "exchange_rates".
 *
 * @property integer $id
 * @property string $currency
 * @property float $rate
 * @property string $date
 */
class ExchangeRate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exchange_rates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate', 'currency', 'date'], 'required'],
            [['rate'], 'number'],
            [['currency'], 'string', 'max' => 3],
            [['date'], 'date', 'format' => 'd.m.Y'],
            ['date', 'filter', 'filter' => function ($value) {
                return date('Y-m-d', strtotime($value));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency' => 'Currency',
            'rate' => 'Rate',
            'date' => 'Date',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            return $this->checkExistsExchange() ? false : true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function checkExistsExchange()
    {
        return self::find()
            ->where([
                'currency' => $this->currency,
                'date' => $this->date
            ])
            ->exists();
    }
}
