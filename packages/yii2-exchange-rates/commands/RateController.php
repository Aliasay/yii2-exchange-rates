<?php

namespace galiasay\exchange\commands;

use galiasay\exchange\models\ExchangeRate;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\web\BadRequestHttpException;

class RateController extends Controller
{
    /**
     * Load exchange rates.
     *
     * @param null $date Date in format: 'Ymd'
     * @param null $rate Currency code
     */
    public function actionLoad($date = null, $rate = null)
    {
        try {
            $params = [
                'date' => $date,
                'valcode' => $rate ?: $this->module->currency
            ];

            $content = file_get_contents($this->module->url . '?' . http_build_query($params));
            $content = simplexml_load_string($content);

            $model = new ExchangeRate([
                'rate' => (float) $content->currency->rate,
                'currency' => (string) $content->currency->cc,
                'date' => (string) $content->currency->exchangedate,
            ]);

            $model->save();

            if ($model->hasErrors()) {
                $this->printErrors($model->firstErrors);
            }

        } catch (ErrorException $e) {
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * Display errors.
     *
     * @param array $errors
     */
    private function printErrors(array $errors)
    {
        foreach ($errors as $field => $error) {
            echo "{$field}: $error\n";
        }
    }
}
