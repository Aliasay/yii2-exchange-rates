<?php

namespace galiasay\exchange\widgets;

use app\assets\ChartAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

class Chart extends Widget
{
    /**
     * @var array
     */
    public $elementOptions = [];

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var string
     */
    public $type;

    /**
     * @var array
     */
    public $data = [];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        if ($this->type === null) {
            throw new InvalidConfigException("The 'type' option is required");
        }

        if (!isset($this->elementOptions['id'])) {
            $this->elementOptions['id'] = $this->getId();
        }
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo Html::tag('canvas', '', $this->elementOptions);
        $this->registerPlugin();
    }

    /**
     * Register ChartJs plugin.
     */
    protected function registerPlugin() {
        $id = $this->elementOptions['id'];

        $view = $this->getView();
        ChartAsset::register($view);

        $config = Json::encode([
            'type' => $this->type,
            'data' => $this->data ?: new JsExpression('{}'),
            'options' => $this->options ?: new JsExpression('{}')
        ]);

        $js = "var chartJS_{$id} = new Chart($('#{$id}'), {$config});";
        $view->registerJs($js);
    }
}